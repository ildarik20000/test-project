import 'package:intl/intl.dart';

class ParseData {
  String parseTimeDayTitle(DateTime dateTime) {
    return DateFormat.yMMMMd('en_US').format(dateTime).toString();
  }

  String parseTimeInfo(DateTime dateTime) {
    return DateFormat.jm().format(dateTime).toString();
  }
}
