import 'package:flutter/material.dart';

class MyCustomClipper extends CustomClipper<Path> {
  double marginTop = 0;
  MyCustomClipper({this.marginTop = 0});
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, 55);

    var firstControlPoint = Offset(size.width / 4, 0.0);
    var firstEndPoint = Offset(size.width / 2.25, 45.0);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondControlPoint = Offset(size.width - (size.width / 3.25), 105);
    var secondEndPoint = Offset(size.width, 45);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    path.lineTo(size.width, size.height);
    path.lineTo(0.0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
