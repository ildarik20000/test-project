import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_project/config/theme.dart';
import 'package:test_project/customicons_icons.dart';

class CardHistory extends StatelessWidget {
  String dateTitle;
  String time;
  String contacts;
  Function func;

  CardHistory(this.dateTitle, this.contacts, this.time, this.func);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 310,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(right: 25),
                child: Icon(
                  Icons.backup_outlined,
                  size: 27,
                ),
              ),
              Container(
                child: Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 30,
                        alignment: Alignment.centerLeft,
                        child: Text(
                          dateTitle,
                          textAlign: TextAlign.start,
                          style: GoogleFonts.montserrat(
                              fontSize: 17,
                              fontWeight: FontWeight.w500,
                              color: TestColors.title),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Text(
                          time + " • " + contacts,
                          textAlign: TextAlign.start,
                          style: GoogleFonts.montserrat(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              color: TestColors.text),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: func,
                child: Container(
                  alignment: Alignment.center,
                  child: Icon(
                    Customicons.export_card,
                    color: TestColors.purple,
                    size: 27,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
