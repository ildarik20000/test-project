import 'package:flutter/material.dart';
import 'package:test_project/config/theme.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_project/customicons_icons.dart';
import 'package:test_project/screens/main.dart';
import 'package:test_project/shared/blockInfo-subcription.dart';

class Subscription extends StatelessWidget {
  Subscription({Key key}) : super(key: key);
  final LinearGradient linearGradient = LinearGradient(
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
    colors: <Color>[
      TestColors.gradient1.withOpacity(1),
      TestColors.gradient3.withOpacity(0.75),
    ],
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [_header(context), _body(context), _bottom()],
        ),
      ),
    );
  }

  Widget _header(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(14),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: IconButton(
                icon: Icon(
                  Icons.cancel,
                  color: Color.fromRGBO(0, 0, 0, 0.4),
                  size: 30,
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MainScreen()),
                  );
                }),
          ),
          Container(
            alignment: Alignment.bottomRight,
            child: Text(
              "Restore Purchases",
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                  color: TestColors.purple,
                  fontSize: 15,
                  fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
    );
  }

  Widget _body(BuildContext context) {
    return Container(
      child: Expanded(
        child: Column(
          children: [
            Container(
              width: 330,
              child: Column(
                children: [
                  Container(
                    child: Text(
                      "Contacts Backup",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                          color: TestColors.title,
                          fontSize: 26,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  Center(
                    child: ShaderMask(
                      blendMode: BlendMode.srcIn,
                      shaderCallback: (Rect bounds) {
                        return linearGradient.createShader(bounds);
                      },
                      child: Text(
                        "Pro",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.montserrat(
                          fontSize: 32,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Container(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            child: Center(
                              child: Text(
                                "We understand that your contacts are important for you, so we have developed Contact Backup Master to keep your address book secure at all times",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.montserrat(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    color: TestColors.text),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 30),
                            child: Column(
                              children: [
                                Container(
                                  child: blockInfoSubscription(
                                    icons: Customicons.refresh,
                                    text: "Automatically back up your contacts",
                                    title: "Automatic Backups",
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: blockInfoSubscription(
                                    icons: Icons.cloud_queue,
                                    text:
                                        "Store your backups securely in the cloud",
                                    title: "Cloud Storage",
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: blockInfoSubscription(
                                    icons: Customicons.shield,
                                    text:
                                        "Your cloud backups will be securely protected",
                                    title: "Backups Protection",
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _bottom() {
    return Container(
      padding: EdgeInsets.only(left: 17, right: 17, bottom: 10),
      child: Column(
        children: [
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Text(
                  "Free unlimited access",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: TestColors.text),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(bottom: 10),
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: linearGradient),
                child: Text(
                  "Get Master version",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: Colors.white),
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.bottomRight,
                child: Text(
                  "Terms of Use",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      color: TestColors.purple,
                      fontSize: 15,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                child: Text(
                  "Privacy policy",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      color: TestColors.purple,
                      fontSize: 15,
                      fontWeight: FontWeight.w400),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
