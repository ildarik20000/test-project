import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_project/config/theme.dart';
import 'package:test_project/shared/custom-clipper.dart';

class Settings extends StatelessWidget {
  Settings({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 55),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            alignment: Alignment.topCenter,
            child: Center(
              child: Text(
                "Settings",
                textAlign: TextAlign.center,
                style: GoogleFonts.montserrat(
                    fontSize: 26,
                    fontWeight: FontWeight.w600,
                    color: TestColors.title),
              ),
            ),
          ),
          Expanded(
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 40),
                  alignment: Alignment.bottomCenter,
                  child: ClipPath(
                    clipper: MyCustomClipper(),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: TestColors.linearGradientGray),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 170),
                  alignment: Alignment.center,
                  child: ListView(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: Text(
                          "Help",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                              color: TestColors.purple),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 35),
                        alignment: Alignment.center,
                        child: Text(
                          "Privacy Policy",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                              color: TestColors.purple),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 35),
                        alignment: Alignment.center,
                        child: Text(
                          "Terms of Use",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                              color: TestColors.purple),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 35),
                        alignment: Alignment.center,
                        child: Text(
                          "Support",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              fontSize: 24,
                              fontWeight: FontWeight.w600,
                              color: TestColors.purple),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
