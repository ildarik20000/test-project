import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_project/config/theme.dart';
import 'package:test_project/customicons_icons.dart';
import 'package:test_project/screens/backup.dart';
import 'package:test_project/screens/history.dart';
import 'package:test_project/screens/setting.dart';
import 'package:test_project/shared/button-menu.dart';

class MainScreen extends StatefulWidget {
  int activeButton;
  bool dialog;
  MainScreen({this.activeButton = 1, this.dialog = true});

  @override
  _MainScreenState createState() => _MainScreenState(activeButton, dialog);
}

class _MainScreenState extends State<MainScreen> {
  int activeButton;
  bool dialog = true;
  _MainScreenState(this.activeButton, this.dialog);
  int _stars = 0;

  @override
  void initState() {
    super.initState();
    this.widget.dialog
        ? WidgetsBinding.instance.addPostFrameCallback(
            (_) => showDialog<String>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                backgroundColor: Color.fromRGBO(242, 242, 242, 0.8),
                title: Column(
                  children: [
                    Center(
                      child: Text(
                        "Enjoying Contacts Backup Pro",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.montserrat(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: TestColors.title),
                      ),
                    ),
                    Center(
                      child: Text(
                        "Tap a star to rate it on the App Store.",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.montserrat(
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                            color: TestColors.title),
                      ),
                    ),
                  ],
                ),
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    _buildStar(1),
                    _buildStar(2),
                    _buildStar(3),
                    _buildStar(4),
                    _buildStar(5),
                  ],
                ),
                actions: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      FlatButton(
                        child: Text('Cancel'),
                        onPressed: Navigator.of(context).pop,
                      ),
                      FlatButton(
                        child: Text('Submit'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        : null;
  }

  Widget _buildStar(int starCount) {
    return InkWell(
      child: Icon(
        Icons.star_border_outlined,
        size: 30.0,
        color: _stars >= starCount ? Colors.blue : Colors.grey,
      ),
      onTap: () {
        setState(() {
          _stars = starCount;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          shape: CircularNotchedRectangle(),
          child: Container(
            height: 78,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ButtonMenu(
                  icon: (Customicons.history),
                  onPress: () {
                    setState(() {
                      activeButton = 0;
                    });
                  },
                  text: 'History',
                  active: activeButton == 0,
                  disabled: activeButton == 0,
                ),
                ButtonMenu(
                  icon: (Icons.backup_outlined),
                  onPress: () {
                    setState(() {
                      activeButton = 1;
                    });
                  },
                  text: 'Back Up',
                  active: activeButton == 1,
                  disabled: activeButton == 1,
                ),
                ButtonMenu(
                  icon: (Customicons.setting),
                  onPress: () {
                    setState(() {
                      activeButton = 2;
                    });
                  },
                  text: 'Settings',
                  active: activeButton == 2,
                  disabled: activeButton == 2,
                ),
              ],
            ),
          ),
        ),
        body: Container(
          child: Stack(alignment: Alignment.bottomCenter, children: [
            Offstage(
              offstage: activeButton != 0,
              child: History(),
            ),
            Offstage(
              offstage: activeButton != 1,
              child: Container(
                child: BackUp(),
              ),
            ),
            Offstage(
              offstage: activeButton != 2,
              child: Settings(),
            ),
          ]),
        ));
  }
}
