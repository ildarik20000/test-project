import 'dart:io';

import 'package:contacts_service/contacts_service.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:test_project/config/theme.dart';
import 'package:test_project/models/backup.dart';
import 'package:test_project/models/contact.dart';
import 'package:test_project/models/user.dart';
import 'package:test_project/screens/main.dart';
import 'package:test_project/shared/custom-clipper.dart';

class BackUp extends StatefulWidget {
  BackUp();

  User user = User();
  double progress = 0;
  String titleText = "Contacts Backup";
  String infoText =
      "Backup your address book to avoid losing your contacts in case something happens to your phone.";
  String textButton = "Create Backup";
  bool tapButton = false;
  bool loadingDataCancel = false;
  Color textColor = TestColors.title;

  _BackUpState createState() => _BackUpState();
}

PermissionStatus permissionStatus;

class _BackUpState extends State<BackUp> {
  Iterable<Contact> _contacts;

  @override
  void initState() {
    super.initState();
    getContacts();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          _body(),
          Container(margin: EdgeInsets.only(top: 55), child: _header()),
        ],
      ),
    );
  }

  Widget _header() {
    return Container(
      alignment: Alignment.topCenter,
      height: 200,
      child: Column(
        children: [
          Container(
            child: Text(
              this.widget.titleText,
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                  fontSize: 26,
                  fontWeight: FontWeight.w600,
                  color: this.widget.textColor),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 26, left: 12, right: 12),
            child: Text(
              this.widget.infoText,
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: this.widget.textColor),
            ),
          ),
        ],
      ),
    );
  }

  Widget _body() {
    return this.widget.loadingDataCancel
        ? Container(
            decoration: BoxDecoration(gradient: TestColors.linearGradientOnTap),
            child: Stack(
              alignment: AlignmentDirectional.bottomStart,
              children: [
                Center(
                  child: Text(
                    this.widget.textButton,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.montserrat(
                        fontSize: 28,
                        fontWeight: FontWeight.w900,
                        color: Colors.white),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainScreen(
                                  activeButton: 0,
                                  dialog: false,
                                )),
                      );
                    });
                  },
                  child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(left: 22, right: 22, bottom: 22),
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white),
                    child: Text(
                      "Open Backup",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color: TestColors.purple),
                    ),
                  ),
                ),
              ],
            ),
          )
        : Container(
            child: Stack(
              children: [
                Container(
                  alignment: Alignment.bottomCenter,
                  child: ClipPath(
                    clipper: MyCustomClipper(),
                    child: Container(
                      height: MediaQuery.of(context).size.height / 2,
                      decoration: BoxDecoration(
                          gradient: this.widget.tapButton
                              ? TestColors.linearGradientOnTap
                              : TestColors.linearGradientGray),
                    ),
                  ),
                ),
                Stack(
                  children: [
                    Center(
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        curve: Curves.fastOutSlowIn,
                        width: 246 + this.widget.progress,
                        height: 246 + this.widget.progress,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(256)),
                      ),
                    ),
                    Center(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            if (permissionStatus == PermissionStatus.granted) {
                              this.widget.titleText = "Backing Up..";
                              this.widget.infoText =
                                  "Please sit tight while your contacts are being backed up.";
                              this.widget.tapButton = true;
                            }
                          });
                          if (this.widget.textButton == "100%") {
                            this.widget.loadingDataCancel = true;
                            this.widget.titleText = "Completed!";
                            this.widget.infoText =
                                "Your backup has been completed and saved to your archive.";
                            this.widget.textColor = Colors.white;
                            context
                                .read<BackUpList>()
                                .backupList
                                .add(this.widget.user);
                          }

                          _updateTextLoading();
                        },
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.fastOutSlowIn,
                          alignment: Alignment.center,
                          width: 192 + this.widget.progress,
                          height: 192 + this.widget.progress,
                          decoration: BoxDecoration(
                              gradient: this.widget.tapButton
                                  ? TestColors.linearGradientPurple
                                  : TestColors.linearGradientOnTap,
                              borderRadius: BorderRadius.circular(256)),
                          child: Text(
                            this.widget.textButton,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.montserrat(
                                fontSize: 28,
                                fontWeight: FontWeight.w900,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
  }

  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
          await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ??
          PermissionStatus.undetermined;
    } else {
      return permission;
    }
  }

  Future<void> getContacts() async {
    final Iterable<Contact> contacts = await ContactsService.getContacts();
    permissionStatus = await _getPermission();
    setState(() {
      _contacts = contacts;
    });
  }

  _updateTextLoading() {
    this.widget.user = User();
    double plusWidth = 86 / _contacts.length ?? 1;
    double plusButtonProgress = 100 / _contacts.length ?? 1;
    double progressButton = 0;
    for (int i = 0; i < _contacts.length; i++) {
      //здесь должна происходить анимация круга
      progressButton += plusButtonProgress;
      ContactModel contact = ContactModel(_contacts.elementAt(i));
      //setState(() {
      this.widget.textButton = progressButton.toString() + "%";
      this.widget.progress += plusWidth;

      this.widget.user.contacts.add(contact);
      //});
    }
    //После завершения открытие конечного экрана
    //sleep(Duration(seconds: 1));
    this.widget.textButton = "100%";
    this.widget.user.dataCreat = DateTime.now();
  }
}
