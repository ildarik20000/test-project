import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:test_project/config/theme.dart';
import 'package:test_project/models/backup.dart';
import 'package:test_project/models/user.dart';
import 'package:test_project/services/parse_date.dart';
import 'package:test_project/shared/card-history.dart';

class History extends StatefulWidget {
  History({Key key}) : super(key: key);
  List<User> backUps;
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    loadData();
    return Container(
      margin: EdgeInsets.only(top: 55),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Container(
              child: Text(
                "History",
                textAlign: TextAlign.center,
                style: GoogleFonts.montserrat(
                    fontSize: 26,
                    fontWeight: FontWeight.w600,
                    color: TestColors.title),
              ),
            ),
          ),
          Divider(),
          this.widget.backUps.length != 0
              ? Container(
                  child: Expanded(
                  child: ListView.builder(
                      itemCount: this.widget.backUps.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 25, right: 25),
                              child: CardHistory(
                                ParseData().parseTimeDayTitle(
                                    this.widget.backUps[index].dataCreat),
                                this
                                        .widget
                                        .backUps[index]
                                        .contacts
                                        .length
                                        .toString() +
                                    " contacts",
                                ParseData().parseTimeInfo(
                                    this.widget.backUps[index].dataCreat),
                                () {},
                              ),
                            ),
                            Divider(),
                          ],
                        );
                      }),
                ))
              : Container(),
        ],
      ),
    );
  }

  loadData() {
    this.widget.backUps = Provider.of<BackUpList>(context).backupList ?? [];
  }
}
