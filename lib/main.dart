import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_project/models/backup.dart';

import 'screens/subscription.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider<BackUpList>.value(
      value: BackUpList(),
      child: MaterialApp(
        home: Subscription(),
      ),
    );
  }
}
