import 'package:flutter/material.dart';

class TestColors {
  static Color purple = Color.fromRGBO(100, 101, 249, 1);
  static Color gradient1 = Color.fromRGBO(86, 243, 196, 1);
  static Color gradient2 = Color.fromRGBO(86, 243, 196, 0);
  static Color gradient3 = Color.fromRGBO(77, 74, 249, 1);
  static Color gradient4 = Color.fromRGBO(0, 0, 0, 1);
  static Color gray = Color.fromRGBO(243, 243, 243, 1);

  static Color title = Color.fromRGBO(0, 0, 0, 1);
  static Color text = Color.fromRGBO(141, 141, 141, 1);

  static LinearGradient linearGradientGray = LinearGradient(
    begin: Alignment.bottomRight,
    end: Alignment.topCenter,
    colors: <Color>[
      TestColors.gray,
      TestColors.gray,
    ],
  );

  static LinearGradient linearGradientOnTap = LinearGradient(
    begin: Alignment.bottomRight,
    end: Alignment.topCenter,
    colors: <Color>[
      TestColors.gradient1,
      TestColors.gradient3,
    ],
  );

  static LinearGradient linearGradientPurple = LinearGradient(
    begin: Alignment.bottomRight,
    end: Alignment.topCenter,
    colors: <Color>[
      TestColors.purple,
      TestColors.purple,
    ],
  );
}
