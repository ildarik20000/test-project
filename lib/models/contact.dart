import 'dart:typed_data';
import 'dart:ui';
import 'package:contacts_service/contacts_service.dart';
import 'package:path_provider/path_provider.dart';

class ContactModel {
  String displayName, givenName, middleName, prefix, suffix, familyName;

  String company, jobTitle;

  Iterable<Item> emails = [];

  Iterable<Item> phones = [];

  Iterable<PostalAddress> postalAddresses = [];

  Uint8List avatar;

  ContactModel(Contact contact) {
    this.displayName = contact.displayName;
    this.givenName = contact.givenName;
    this.middleName = contact.middleName;
    this.prefix = contact.prefix;
    this.suffix = contact.suffix;
    this.familyName = contact.familyName;
    this.company = contact.company;
    this.jobTitle = contact.jobTitle;
    this.emails = contact.emails;
    this.phones = contact.phones;
    this.postalAddresses = contact.postalAddresses;
    this.avatar = contact.avatar;
  }
  Map<String, dynamic> toMap() {
    return {
      "displayName": this.displayName,
      "givenName": this.givenName,
      "middleName": this.middleName,
      "prefix": this.prefix,
      "suffix": this.suffix,
      "familyName": this.familyName,
      "company": this.company,
      "jobTitle": this.jobTitle,
      "emails": this.emails,
      "phones": this.phones,
      "postalAddresses": this.postalAddresses,
      "avatar": this.avatar,
    };
  }

  ContactModel.fromJson(Map<String, dynamic> data) {
    displayName = data['displayName'];
    givenName = data['givenName'];
    middleName = data['middleName'];
    prefix = data['prefix'];
    suffix = data['suffix'];
    familyName = data['familyName'];
    company = data['company'];
    jobTitle = data['jobTitle'];
    emails = data['emails'];
    phones = data['phones'];
    postalAddresses = data['postalAddresses'];
    avatar = data['avatar'];
  }
}
