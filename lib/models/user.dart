import 'package:test_project/models/contact.dart';

class User {
  DateTime dataCreat;
  List<ContactModel> contacts = [];

  User();

  Map<String, dynamic> toMap() {
    return {"contacts": contacts.map((c) => c.toMap()).toList()};
  }

  User.fromJson(Map<String, dynamic> data) {
    if (data["contacts"] != null)
      contacts = List<ContactModel>.from(
          data["contacts"].map((i) => ContactModel.fromJson(i)));
  }
}
